FROM openjdk:17-alpine
COPY build/libs/ms14-0.0.1-SNAPSHOT.jar /docker_project-app
ENTRYPOINT ["java"]
CMD ["-jar", "/docker_project-app/ms14-0.0.1-SNAPSHOT.jar"]