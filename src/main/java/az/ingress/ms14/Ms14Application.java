package az.ingress.ms14;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ms14Application implements CommandLineRunner {
	
	public static void main(String[] args) {
		SpringApplication.run(Ms14Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

	}
}
