package az.ingress.ms14.controller;

import az.ingress.ms14.dto.CreateStudentDto;
import az.ingress.ms14.dto.UpdateStudentDto;
import az.ingress.ms14.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;
import az.ingress.ms14.dto.StudentDto;
@RestController
@RequestMapping("/student")
@RequiredArgsConstructor
public class StudentController {
    private final StudentService studentService;

    @PostMapping
    public void create(@RequestBody CreateStudentDto studentDto){
      studentService.create(studentDto);
    }

    @PutMapping
    public void update(@RequestBody UpdateStudentDto studentDto){
        studentService.update(studentDto);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id){
        studentService.delete(id);
    }

    @GetMapping("/{id}")
    public Optional<StudentDto> get(@PathVariable Integer id){
        return studentService.get(id);
    }

    @GetMapping("/find-all")
    public List<StudentDto> getAll(StudentDto studentDto){
        return studentService.getStudentList();
    }

}
