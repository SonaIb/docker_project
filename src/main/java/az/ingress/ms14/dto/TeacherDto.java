package az.ingress.ms14.dto;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import java.time.LocalDate;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TeacherDto {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    String name;
    String faculty;
    LocalDate birthdate;

}
