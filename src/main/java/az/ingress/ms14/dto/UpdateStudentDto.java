package az.ingress.ms14.dto;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UpdateStudentDto {

    Integer id;
    String name;
    String surname;

}
