package az.ingress.ms14.model;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Builder
@AllArgsConstructor //builder -e gore teleb olunur
@NoArgsConstructor // buildere gore teleb olunur
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Integer id;
    String name;
    String surname;
    String password;
}
