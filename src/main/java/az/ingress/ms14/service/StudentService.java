package az.ingress.ms14.service;
import az.ingress.ms14.dto.CreateStudentDto;
import az.ingress.ms14.dto.StudentDto;
import az.ingress.ms14.dto.UpdateStudentDto;
import az.ingress.ms14.model.Student;
import az.ingress.ms14.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.ArrayList;
import jakarta.persistence.EntityNotFoundException;
import java.util.stream.Collectors;
@Service
@RequiredArgsConstructor
public class StudentService {
private final StudentRepository studentRepository;
private final ModelMapper modelMapper;


    public void create(CreateStudentDto dto) {
        Student student = modelMapper.map(dto, Student.class);
        studentRepository.save(student);
    }

    public void update(UpdateStudentDto dto) {
        Optional<Student> entity =  studentRepository.findById(dto.getId());
        if (entity.isPresent()) {
            Student student = entity.get();
            modelMapper.map(dto, student);
            studentRepository.save(student);
        } else {
            throw new EntityNotFoundException("Student not found with id: " + dto.getId());
        }
    }

    public void delete(Integer id) {
        studentRepository.deleteById(id);
    }
    public Optional<StudentDto> get(Integer id) {
        Optional<Student> student = studentRepository.findById(id);
        StudentDto studentDto = student.map(s -> modelMapper.map(s, StudentDto.class)).orElse(null);
        return Optional.ofNullable(studentDto);
    }

    public List<StudentDto> getStudentList(){
       List<StudentDto> studentDtos = studentRepository.findAll().stream().
                map(student -> modelMapper.map(student, StudentDto.class))
                .collect(Collectors.toList());
        return studentDtos;
    }

}
